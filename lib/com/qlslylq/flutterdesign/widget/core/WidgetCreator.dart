import 'package:flutter/material.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/resource/StringRes.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/util/Log.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/util/TextUtils.dart';

/*
 * 组件库 <br/>
 * 用于创建常用的组件<br/>
 */
class WidgetCreator {

  /*
   * 创建公有网络进度栏<br/>
   */
  static Widget createCommonProgressBar(
      {String text = StringRes.progressbar_text}) {
    Widget progressBar = new Container(
      width: 198,
      height: 63,
      margin: EdgeInsets.fromLTRB(0, 0, 0, 110),
      decoration: BoxDecoration(
          color: Color.fromARGB(127, 127, 127, 127),
          border: Border.all(color: Colors.blue, width: 0.1),
          borderRadius: BorderRadius.circular(4)),
      child: new Row(children: <Widget>[
        new CircularProgressIndicator(),
        new Container(
          width: 115,
          child: new Text(
            TextUtils.isEmpty(text) ? StringRes.progressbar_text : text,
            textScaleFactor: 1.1,),),
      ], mainAxisAlignment: MainAxisAlignment.spaceEvenly,),);

    return progressBar;
  }
}