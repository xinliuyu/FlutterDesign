/*
 * Log统一管理类<br/>
 */
class Log {

  /*
   * 默认的输出标签
   */
  static final String TAG = "输出";

  static final bool isDebug = true;

  static void v(String tag, Object msg) {
    if (isDebug) {
      recordLogVerbose(tag, msg);
    }
  }

  static void d(String tag, Object msg) {
    if (isDebug) {
      recordLogDebug(tag, msg);
    }
  }

  static void i(String tag, Object msg) {
    if (isDebug) {
      recordLogInfo(tag, msg);
    }
  }

  static void w(String tag, Object msg) {
    if (isDebug) {
      recordLogWarn(tag, msg);
    }
  }

  static void e(String tag, Object msg) {
    if (isDebug) {
      recordLogError(tag, msg);
    }
  }

  static void ln() {
    if (isDebug) {
      print("");
    }
  }

  static void recordLogVerbose(String tag, Object msg) {
    if (msg == null) {
      msg = "null";
    }
    String result = msg.toString().replaceAll("<br/>", "\n") + "\n";
    print('${tag}:  ${result}');
  }

  static void recordLogDebug(String tag, Object msg) {
    if (msg == null) {
      msg = "null";
    }
    String result = msg.toString().replaceAll("<br/>", "\n") + "\n";
    print('${tag}:  ${result}');
  }

  static void recordLogInfo(String tag, Object msg) {
    if (msg == null) {
      msg = "null";
    }
    String result = msg.toString().replaceAll("<br/>", "\n") + "\n";
    print('${tag}:  ${result}');
  }

  static void recordLogWarn(String tag, Object msg) {
    if (msg == null) {
      msg = "null";
    }
    String result = msg.toString().replaceAll("<br/>", "\n") + "\n";
    print('${tag}:  ${result}');
  }

  static void recordLogError(String tag, Object msg) {
    if (msg == null) {
      msg = "null";
    }
    String result = msg.toString().replaceAll("<br/>", "\n") + "\n";
    print('${tag}:  ${result}');
  }

}
