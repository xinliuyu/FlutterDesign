import 'package:dio/dio.dart';

//请求类型
enum HttpMethod {
  GET,
  POST
}

//文件类型
enum FileType {
  PICTURE,
  AUDIO
}

/*
 * Http请求操作封装基类 <br/>
 */
abstract class Protocol {

  /*
   * 图片键前缀
   */
  final String KEY_PICTURE = "file";

  /*
   * 音频键前缀
   */
  final String KEY_AUDIO = "file";

  /*
   * 服务
   */
  String service;

  /*
   * method
   */
  String method;

  /*
   * 基本url
   */
  String url;

  String getService() {
    return service;
  }

  Protocol setService(String service) {
    this.service = service;
    return this;
  }

  String getMethod() {
    return method;
  }

  Protocol setMethod(String method) {
    this.method = method;
    return this;
  }

  Protocol setUrl(String url) {
    this.url = url;
    return this;
  }

  /*
   * 外异内同<br/>
   * GET请求<br/>
   */
  Future<Response<Map<String, dynamic>>> get();

  /*
   * 外异内同<br/>
   * POST请求<br/>
   */
  Future<Response<Map<String, dynamic>>> post();

  /*
   * 同步请求,必须异步调用<br/>
   * GET直接请求url<br/>
   */
  Future<Response<Map<String, dynamic>>> request(HttpMethod httpMethod,
      Map<String, dynamic>requestParams);

  /*
   * 添加地址参数<br/>
   */
  Protocol addUrlParam(String value);

  /*
   * 添加参数<br/>
   */
  Protocol addParam(String name, Object value);

  /*
   * 获取基本url与参数的拼接字符串<br/>
   * 只能在网络请求返回之后调用，才能获取准确的参数<br/>
   */
  String getUrl();

  /*
   * 获取参数<br/>
   * 只能在网络请求返回之后调用，才能获取准确的参数<br/>
   */
  Object getParams();

}