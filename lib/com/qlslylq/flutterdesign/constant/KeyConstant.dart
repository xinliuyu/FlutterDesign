/*
 * APP_KEY,APP_ID等均在此定义<br/>
 */
class KeyConstant {

  /*
  * 聚合数据key (不使用聚合SDK时才需要此key,项目已集成聚合SDK,请求次数增加,此key暂无用处)
  */
  static final String APP_KEY_JUHE_TRAIN = "";

  static final String APP_KEY_JUHE_FLIGHT = "";

  /*
  * 微信APP_ID，APP_SECRET
  */
  static final String APP_ID_WX = "";
  static final String APP_SECRET_WX = "";

  /*
  * QQ APP_ID，APP_KEY
  */
  static final String APP_ID_QQ = "";

  static final String APP_KEY_QQ = "";

  /*
  * 应用窝APP_KEY
  */
  static final String APP_KEY_ADMX = "7b0893b20d0a24215bc076b065841885";


}
