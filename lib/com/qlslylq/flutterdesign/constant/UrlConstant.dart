/*
 * Url常量类
 */
class UrlConstant {

  /*
  * 线上服务器基本url
  */
  static final String BASE_URL = "http://api.cloudadmx.com";

  /*
  * 资源url
  */
  static final String RES_URL = BASE_URL;

}
